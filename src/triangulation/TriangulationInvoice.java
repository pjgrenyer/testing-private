package triangulation;

import java.util.ArrayList;
import java.util.List;

import common.Invoice;
import common.LineItem;


public class TriangulationInvoice implements Invoice
{
	private final List<LineItem> lineItems = new ArrayList<LineItem>();
	private final double vatPc;
	
	public TriangulationInvoice(double vatPc)
	{
		this.vatPc = vatPc;
	}
	
	@Override
	public void add(LineItem lineItem)
	{
		lineItems.add(lineItem);
	}
	
	@Override
	public double grossTotal()
	{
		double total = 0;
		for(LineItem lineItem : lineItems)
			total += lineItem.getValue();
		return total;
	}
	
	@Override
	public double netTotal()
	{
		return addVat( grossTotal() );
	}
	
	private double addVat(double value)
	{
		return vatmultiplier() * value;
	}
	
	private double vatmultiplier()
	{
		return (100 + vatPc) / 100;
	}
}
