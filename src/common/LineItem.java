package common;

public class LineItem 
{
	private final double value;
	
	public LineItem(double value) 
	{
		this.value = value;
	}
	
	public double getValue()
	{
		return value;
	}

}
