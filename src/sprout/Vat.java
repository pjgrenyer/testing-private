package sprout;

public class Vat 
{
	private final double rate;
	
	public Vat(double rate) 
	{
		this.rate = rate;
	}
	
	public double add(double value)
	{
		return multiplier() * value;
	}
	
	private double multiplier()
	{
		return (100 + rate) / 100;
	}
}
