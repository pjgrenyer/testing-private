package sprout;

import java.util.ArrayList;
import java.util.List;

import common.Invoice;
import common.LineItem;


public class SproutInvoice implements Invoice
{
	private final List<LineItem> lineItems = new ArrayList<LineItem>();
	private final double vatPc;
	
	public SproutInvoice(double vatPc)
	{
		this.vatPc = vatPc;
	}
	
	@Override
	public void add(LineItem lineItem)
	{
		lineItems.add(lineItem);
	}
	
	@Override
	public double grossTotal()
	{
		double total = 0;
		for(LineItem lineItem : lineItems)
			total += lineItem.getValue();
		return total;
	}
	
	@Override
	public double netTotal()
	{
		return new Vat(vatPc).add( grossTotal() );
	}
}
