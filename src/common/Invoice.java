package common;

public interface Invoice 
{
	void add(LineItem lineItem);
		
	double grossTotal();
	
	double netTotal();
}
