package sprout;

import common.AbstractInvoiceTest;
import common.Invoice;

public class InvoiceTest extends AbstractInvoiceTest
{
	@Override
	protected Invoice createInvoice(double vatRate) 
	{
		return new SproutInvoice(vatRate);
	}	
}
