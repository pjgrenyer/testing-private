package sprout;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class VatTest 
{
	@Test
	public void vatAt20pc() 
	{
		assertEquals(120, new Vat(20).add(100), 1);
	}
	
	@Test
	public void vatAt15pc() 
	{
		assertEquals(115, new Vat(15).add(100), 1);
	}
	
	@Test
	public void vatAt17_5pc() 
	{
		assertEquals(117.5, new Vat(17.5).add(100), 1);
	}
	
	@Test
	public void vatAt0pc() 
	{
		assertEquals(100, new Vat(0).add(100), 1);
	}
	
	@Test
	public void vatAt100pc() 
	{
		assertEquals(200, new Vat(100).add(100), 1);
	}
}
