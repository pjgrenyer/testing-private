package triangulation;

import common.AbstractInvoiceTest;

public class InvoiceTest extends AbstractInvoiceTest
{
	@Override
	protected TriangulationInvoice createInvoice(double vatRate) 
	{
		return new TriangulationInvoice(vatRate);
	}	
}
