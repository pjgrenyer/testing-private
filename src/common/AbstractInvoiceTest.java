package common;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

public abstract class AbstractInvoiceTest
{
	private static final double VAT_PC = 20;
		
	private Invoice invoice;
	
	@Before
	public void setUp()
	{
		invoice = createInvoice(VAT_PC);
	}

	@Test
	public void totalGrossNoLineItemsIsZero() 
	{
		assertEquals(0, invoice.grossTotal(), 0);
	}
	
	@Test
	public void totalGrossWithLineItems() 
	{
		invoice.add(new LineItem(50));
		invoice.add(new LineItem(20));
		invoice.add(new LineItem(20));
		invoice.add(new LineItem(10));
		assertEquals(100, invoice.grossTotal(), 0);
	}
	
	@Test
	public void totalNetNoLineItemsIsZero() 
	{
		assertEquals(0, invoice.netTotal(), 0);
	}
	
	@Test
	public void totalNetWithLineItems() 
	{
		invoice.add(new LineItem(50));
		invoice.add(new LineItem(20));
		invoice.add(new LineItem(20));
		invoice.add(new LineItem(10));
		assertEquals(120, invoice.netTotal(), 0.1);
	}
	
	@Test
	public void totalNetWithLineItemsAndZeroVAT() 
	{
		invoice = createInvoice(0);
		invoice.add(new LineItem(50));
		invoice.add(new LineItem(20));
		invoice.add(new LineItem(20));
		invoice.add(new LineItem(10));
		assertEquals(100, invoice.netTotal(), 0.1);
	}
	
	protected abstract Invoice createInvoice(double vatRate);

}
